DateTime tryDateTimeParse(String dateTimeString) {
  if (dateTimeString != null) {
    return DateTime.parse(dateTimeString);
  } else {
    return null;
  }
}
