import 'package:hasura_connect/hasura_connect.dart';

class ApiService {
  Future<dynamic> query(query, variables) async {
    HasuraConnect connect =
        HasuraConnect('https://ankit-rirev.herokuapp.com/v1/graphql/');
    connect.addHeader(
        'x-hasura-admin-secret', 'hellow40deplores34houston86enfant00house');
    variables = variables != null ? variables : <String, dynamic>{};
    var result = {};
    print("hello");
    try {
      result = await connect.query(
        query,
        variables: {},
      );
    } on HasuraError catch (err) {
      print("Hasura query error: ${err.message}");
    }
    return result;
  }

  Future<dynamic> mutation(query, variables) async {
    HasuraConnect connect =
        HasuraConnect('https://ankit-rirev.herokuapp.com/v1/graphql');
    connect.addHeader(
        'x-hasura-admin-secret', 'hellow40deplores34houston86enfant00house');

    variables = variables != null ? variables : <String, dynamic>{};
    var result = {};
    try {
      result = await connect.mutation(query, variables: variables);
    } on HasuraError catch (err) {
      print("Hasura mutation error: ${err.message}");
    }
    return result['data'];
  }
}
