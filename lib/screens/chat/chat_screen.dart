import 'package:flutter/material.dart';
import 'package:myapp/screens/profile/profile.dart';

class ChatScreen extends StatefulWidget {
  final String name;
  final String avatar;
  ChatScreen(this.avatar, this.name);
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        titleSpacing: -15,
        title: Row(children: [
          FlatButton.icon(
              onPressed: () => Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) {
                    return new Profile(widget.avatar, widget.name);
                  })),
              icon: CircleAvatar(backgroundImage: NetworkImage(widget.avatar)),
              label: Text(widget.name,
                  style: TextStyle(fontSize: 18, color: Colors.white)))
        ]),
        actions: <Widget>[
          Icon(Icons.videocam),
          Padding(padding: EdgeInsets.symmetric(horizontal: 10)),
          Icon(Icons.call),
          Padding(padding: EdgeInsets.symmetric(horizontal: 10)),
          Icon(Icons.more_vert),
          Padding(padding: EdgeInsets.symmetric(horizontal: 5)),
        ],
      ),
    );
  }
}
