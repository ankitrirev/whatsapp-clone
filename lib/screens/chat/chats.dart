import 'package:flutter/material.dart';
import 'package:myapp/screens/chat/chat_screen.dart';
import 'package:myapp/model/conversation.dart';
import 'dart:convert';
import 'dart:async';

import 'package:myapp/services/api_service.dart';

Future<Conversation> fetchConversation() async {
  var response = await ApiService().query(
    """
  
  query {
    conversations {
      id
      last_message
      name
      updated_at
      avatar
    }
}
  
  """,
    {},
  );

  if (response) {
    print(json.decode(response.body));
    return null;
  } else {
    print("error");
    return null;
  }
}

class Chats extends StatefulWidget {
  @override
  _ChatsState createState() => _ChatsState();
}

class _ChatsState extends State<Chats> {
  List<Map> data;
  Conversation futureConversation;

  bool isLoading = true;

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance.addPostFrameCallback((timeStamp) async {
      futureConversation = await fetchConversation();
      setState(() {
        isLoading = false;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    if (isLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    } else {
      return Container(
        child: FutureBuilder(
          future: DefaultAssetBundle.of(context).loadString('assets/chat.json'),
          builder: (context, snapshot) {
            var newData = json.decode(snapshot.data.toString());
            return ListView.builder(
              itemBuilder: (BuildContext context, int index) {
                return ListTile(
                  leading: CircleAvatar(
                    backgroundImage: NetworkImage(newData[index]['avatar']),
                  ),
                  title: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Text(newData[index]['name'],
                            style: TextStyle(fontWeight: FontWeight.bold)),
                        Text(
                          newData[index]['time'],
                          style: TextStyle(color: Colors.grey, fontSize: 14.0),
                        )
                      ]),
                  subtitle: Container(
                      padding: EdgeInsets.only(top: 5.0),
                      child: Text(
                        newData[index]['message'],
                        style: TextStyle(color: Colors.grey, fontSize: 15.0),
                      )),
                  onTap: () => Navigator.of(context)
                      .push(MaterialPageRoute(builder: (context) {
                    return new ChatScreen(
                        newData[index]['avatar'], newData[index]['name']);
                  })),
                );
              },
              itemCount: newData == null ? 0 : newData.length,
            );
          },
        ),
      );
    }
  }
}
