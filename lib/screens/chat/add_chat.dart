import 'package:flutter/material.dart';

class AddChat extends StatefulWidget {
  @override
  _AddChatState createState() => _AddChatState();
}

class _AddChatState extends State<AddChat> {
  final _formKey = GlobalKey<FormState>();
  final formController = TextEditingController();

  @override
  void dispose() {
    formController.dispose();
    super.dispose();
  }

  void _handleSubmit() {
    _formKey.currentState.save();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Create New Chat'),
          actions: <Widget>[
            IconButton(icon: Icon(Icons.save), onPressed: _handleSubmit)
          ],
        ),
        body: Container(
            width: double.infinity,
            margin: EdgeInsets.all(10),
            child: Column(
              children: <Widget>[
                Text(
                  'Add New Chat',
                  style: TextStyle(fontSize: 22),
                  textAlign: TextAlign.center,
                ),
                Form(
                    key: _formKey,
                    child: Column(
                      children: <Widget>[
                        TextFormField(
                          decoration: InputDecoration(labelText: 'User Name'),
                          textInputAction: TextInputAction.next,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Enter Name of the User';
                            }
                            return null;
                          },
                        ),
                        TextFormField(
                          decoration: InputDecoration(labelText: 'Message'),
                          textInputAction: TextInputAction.next,
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Enter Last Message';
                            }
                            return null;
                          },
                        ),
                        TextFormField(
                          decoration: InputDecoration(labelText: 'Avatar URL'),
                          textInputAction: TextInputAction.next,
                          onFieldSubmitted: (_) {
                            _handleSubmit();
                          },
                          validator: (value) {
                            if (value.isEmpty) {
                              return 'Enter Image URL';
                            }
                            return null;
                          },
                        ),
                      ],
                    ))
              ],
            )));
  }
}
