import 'package:flutter/material.dart';

class Profile extends StatefulWidget {
  final String name;
  final String avatar;
  Profile(this.avatar, this.name);
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            expandedHeight: 400,
            floating: false,
            pinned: true,
            flexibleSpace: FlexibleSpaceBar(
              title: Text(widget.name),
              background: Stack(fit: StackFit.expand, children: [
                Image.network(
                  widget.avatar,
                  fit: BoxFit.cover,
                )
              ]),
            ),
          ),
          SliverList(
              delegate: SliverChildListDelegate([
            Padding(
              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
              child: Card(
                  child: Column(children: [
                ListTile(
                  title: Text(
                    "Media, links, and docs",
                  ),
                  trailing: Icon(
                    Icons.arrow_forward_ios,
                  ),
                ),
              ])),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
              child: Card(
                  child: Column(children: [
                ListTile(
                  title: Text('Mute Notification'),
                  trailing: Switch(
                    value: false,
                    onChanged: null,
                    inactiveThumbColor: Theme.of(context).primaryColor,
                  ),
                ),
                Divider(),
                ListTile(title: Text('Custom Notifications')),
                Divider(),
                ListTile(title: Text('Media Visibility')),
              ])),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
              child: Card(
                  child: ListTile(
                contentPadding: EdgeInsets.all(15),
                title: Text('Encryption'),
                subtitle: Text(
                    'Message to this chat and calls are secured with end-to-end encryption. Tap to verfiy.'),
                trailing: Icon(
                  Icons.lock,
                  color: Theme.of(context).primaryColor,
                ),
              )),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
              child: Card(
                  child: Column(children: [
                ListTile(
                    title: Text(
                  "About and phone number",
                  textAlign: TextAlign.left,
                  style: new TextStyle(color: Theme.of(context).primaryColor),
                )),
                ListTile(
                  title: Text("Hey there! I am using WhatsApp"),
                  subtitle: Text("15 may"),
                ),
                Divider(),
                ListTile(
                  title: Text("+91 98765 4325"),
                  subtitle: Text('Mobile'),
                ),
              ])),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
              child: Card(
                  child: Column(children: [
                ListTile(
                  leading: Icon(
                    Icons.cancel,
                    color: Colors.red,
                  ),
                  title: Text(
                    "Block",
                    style: TextStyle(color: Colors.red, fontSize: 18),
                  ),
                ),
              ])),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 5),
              child: Card(
                  child: Column(children: [
                ListTile(
                  leading: Icon(
                    Icons.thumb_down,
                    color: Colors.red,
                  ),
                  title: Text(
                    "Report Problem",
                    style: TextStyle(color: Colors.red, fontSize: 18),
                  ),
                ),
              ])),
            ),
          ])),
        ],
      ),
    );
  }
}
