import 'package:myapp/services/helpers.dart';

class Conversation {
  final String id;
  final String lastMessage;
  final String name;
  final String avatar;
  final DateTime updatedAt;

  Conversation({
    this.id,
    this.lastMessage,
    this.name,
    this.avatar,
    this.updatedAt,
  });

  factory Conversation.fromJson(Map<String, dynamic> json) {
    return Conversation(
      id: json['id'],
      lastMessage: json['last_message'],
      name: json['name'],
      avatar: json['avatar'],
      updatedAt: tryDateTimeParse(json['updated_at']),
    );
  }
}
