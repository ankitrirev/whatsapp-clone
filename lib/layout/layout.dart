import 'package:flutter/material.dart';
import 'package:myapp/screens/camera.dart';
import 'package:myapp/screens/chat/chats.dart';
import 'package:myapp/screens/chat/add_chat.dart';
import 'package:myapp/screens/status.dart';
import 'package:myapp/screens/calls.dart';

class Layout extends StatefulWidget {
  @override
  _LayoutState createState() => _LayoutState();
}

class _LayoutState extends State<Layout> with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 4, vsync: this, initialIndex: 1);
  }

  @override
  void dispose() {
    super.dispose();
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Whatsapp'),
        bottom: TabBar(controller: _tabController, tabs: [
          Tab(icon: new Icon(Icons.camera_alt)),
          Tab(text: 'CHATS'),
          Tab(text: 'STATUS'),
          Tab(text: 'CALLS')
        ]),
        actions: <Widget>[
          new Icon(Icons.search),
          new Padding(padding: EdgeInsets.symmetric(horizontal: 5)),
          new PopupMenuButton(
            itemBuilder: (BuildContext context) => <PopupMenuItem<String>>[
              PopupMenuItem(child: Text("New Group")),
              PopupMenuItem(child: Text("New Broadcast")),
              PopupMenuItem(child: Text("Whatsapp Web")),
              PopupMenuItem(child: Text("Starred Messages")),
              PopupMenuItem(child: Text("Settings"))
            ],
          )
        ],
      ),
      body: TabBarView(
          controller: _tabController,
          children: [new Camera(), new Chats(), new Status(), new Calls()]),
      floatingActionButton: new FloatingActionButton(
        onPressed: () =>
            Navigator.of(context).push(MaterialPageRoute(builder: (context) {
          return AddChat();
        })),
        child: new Icon(Icons.message, color: Colors.white),
        backgroundColor: new Color(0xff25D366),
        elevation: 0.7,
      ),
    );
  }
}
